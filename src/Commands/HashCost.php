<?php

namespace Drupal\php_password\Commands;

use Drush\Commands\DrushCommands;

class HashCost extends DrushCommands {

  /**
   * Calculate an ideal hash cost for your hardware.
   *
   * @command password-hash-cost
   * @param $run_time Desired millisecond cost. Ideal values are between 100 and 500.
   */
  public function HashCost($run_time, $options = []) {
    $password = 'I8ASD1\-?Z7cl4EB';
    $cost = 3;

    do {
      ++$cost;
      $start = microtime(TRUE);
      password_hash($password, PASSWORD_DEFAULT, ['cost' => $cost]);
      $time = microtime(TRUE) - $start;
    } while ($time < ($run_time / 1000));

    $this->output()->writeLn('Desired cost: ' . $cost);
  }

}
